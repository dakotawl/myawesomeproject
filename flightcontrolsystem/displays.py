""" Displays for the Cockpit Simulation System """
#from typing import setGUI     #used for a parameter hint
from flightsimexception import *

class Display:
    '''
    This is the base class from which all of our displays will be derived.
    '''
    def __init__(self, setGUI) -> None:
        '''
        This ties to the GUI instance of the Display and allows the GUI's
        update method to modify if needed and set its value.  
        '''
        self.setGUI = setGUI

    def update(self, sensorInput:float) -> None:
        '''
        Uses the value passed in to read a value from the sensor, and displays it.
        It executes the method stored by the constructor.
        '''
        return self.setGUI(sensorInput)

    @staticmethod
    def validate_range(value:int) -> bool:
        return 0 <= value <= 100

class Bar(Display):
    '''
    On the application are 4 bar GUI elements: 2 for engine RPM (left, right),
    and two for engine temperature (left, right).  The one Bar class will be instantiated 4
    times in the FlightSim with different parameters.
    '''
    def __init__(self, setGUI, caution:int, warn:int, limit:int) -> None:
        super().__init__(setGUI)
        self.caution = caution
        self.warn = warn
        self.limit = limit

class Tach_bar(Bar):
    '''
    Returns tach sim values for displaying, sends warnings to computer upon reaching certain values
    '''
    def __init__(self, setGUI, caution: int, warn: int, limit: int) -> None:
        super().__init__(setGUI, caution, warn, limit)
        self.state = 'safe'

    def update(self, value:float) -> None:
        self.setGUI(value)

        match self.state:
            case 'safe':
                if not Display.validate_range:
                    self.state = 'limit'
                    raise FlightSimException('Something has gone wrong with the engine!')
                elif value >= self.caution:
                    self.state = 'caution'
                    raise FlightSimException('Engine RPM is YELLOW')
            case 'caution':
                if value >= self.warn:
                    self.state = 'warn'
                    raise FlightSimException('Engine RPM is REDLINE')
                elif value < self.caution:
                    self.state = 'safe'
            case 'warn':
                if not Display.validate_range:
                    self.state = 'limit'
                    raise FlightSimException('Something has gone wrong with the engine!')
                elif value < self.warn:
                    self.state = 'caution'
                    raise FlightSimException('Engine RPM is YELLOW')

class Fuel_level(Display):
    '''
    Placeholder class for the fuel display, will be expanded but currently just uses base functionality
    '''
    def __init__(self, setGUI) -> None:
        super().__init__(setGUI)
        self.state = 'safe'

    def update(self, value:float) -> None:
        self.setGUI(value)

        match self.state:
            case 'safe':
                if not Display.validate_range(value):
                    self.state = 'caution'
                    if value < 0:
                        raise FlightSimException('Fuel tank is empty')
                    else:
                        raise FlightSimException('Fuel Tank Overfilled')
            case 'caution':
                if Display.validate_range(value):
                    self.state = 'safe'
                    raise FlightSimException('Fuel Tank Low')
            case _:
                raise FlightSimException('Flight Computer Error 223')

class Compass_display(Display):
    '''
    Display class for compass, only main difference from base class is converting return value to a string
    '''
    DIRECTION = "N"

    def __init__(self, setGUI, setGUI2):
        super().__init__(setGUI)
        self.directionGUI = setGUI2

    def update(self, sensorInput: float) -> str:

        if sensorInput <= 22.5 or sensorInput > 337.5:
            Compass_display.DIRECTION = "N"
        
        elif sensorInput > 22.5 and sensorInput <= 67.5:
            Compass_display.DIRECTION = "NE"

        elif sensorInput > 67.5 and sensorInput <= 112.5:
            Compass_display.DIRECTION = "E"

        elif sensorInput > 112.5 and sensorInput <= 157.5:
            Compass_display.DIRECTION = "SE"

        elif sensorInput > 157.5 and sensorInput <= 202.5:
            Compass_display.DIRECTION = "S"

        elif sensorInput > 202.5 and sensorInput <= 247.5:
            Compass_display.DIRECTION = "SW"

        elif sensorInput > 247.5 and sensorInput <= 292.5:
            Compass_display.DIRECTION = "W"

        elif sensorInput > 292.5 and sensorInput <= 337.5:
            Compass_display.DIRECTION = "NW"

        return str(self.setGUI(sensorInput)), self.directionGUI(Compass_display.DIRECTION)