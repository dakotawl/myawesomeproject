""" The Sensors class is often called a wrapper which
provides a common software intface to different types
of hardware sensors.  Each type of device will currently
connect to a simulator, but in the near future they will
individually connect to hardware
 """

from simulations import *

class Sensors:
    '''
    This is the base class from which all of our sensors will be derived,
    so it has little functionality other than being a common definition for the interface
    '''
    def __init__(self, Simulator=None) -> None:
        '''
        utilizes a link to the source of inputs, this is usually hardware,
        but in our case, we provide an optional parameter to use as input.
        '''
        self.simulator = Simulator

    def read_sensor(self, values:dict=None) -> float:
        '''
        the first parameter is the dictionary of values from the event loop.  
        This is an optional parameter (not needed when connecting to hardware).
        '''
        if self.simulator is not None:
            return self.simulator.update(values)

class Tach_sensor(Sensors):
    '''
    The tachometer sensor will be derived from the Sensors class and need to override the
    base class read_sensor() method so that it can return a useful value for the simulation.
    '''
    TACH_SCALE_FACTOR = 0.025

    def read_sensor(self, values:dict) -> float:
        returnValue = self.simulator.update(values) * Tach_sensor.TACH_SCALE_FACTOR
        return returnValue

class Tempr_sensor(Sensors):
    '''
    The engine temperature sensor will be derived from the Sensors class and need to
    override the base class read_sensor() method so that it can return a useful value for the simulation.
    '''
    TEMPR_SCALE_FACTOR = 75.0 / 1.3
    OFFSET = -20

    def read_sensor(self, values:dict) -> float:
        returnValue = (self.simulator.update(values) * Tempr_sensor.TEMPR_SCALE_FACTOR) - Tempr_sensor.OFFSET
        return returnValue

class Fuel_sensor(Sensors):
    '''
    Placeholder class for the fuel sensor, will be expanded but currently just uses base class functionality
    '''
#    def read_sensor(self, values:dict) -> float:
#        pass
    @staticmethod
    def convertFuel(pounds:float) -> float:
        gallons = pounds / 6.5
        return gallons

class Compass_sensor(Sensors):
    '''
    Sensor class for the compass, doesn't do much beyond base functionality at the moment.
    '''
    def read_sensor(self, values:dict=None) -> float:
        return self.simulator.update(values)

if __name__ == "__main__":
    print(Fuel_sensor.convertFuel(100))