"""The primary exception for the flight sim applicaton"""
class FlightSimException(Exception):
    '''
    The sensors can determine error conditions which need to be logged immediately to the flight computer
    '''
    def __init__(self, *args: object) -> None:
        super().__init__(*args)

        if args[0] is not None:
            self.message = args[0]

class SensorException(Exception):
    '''
    Raises an exception if there's an issue with the sensors
    '''
    def __init__(self, *args: object) -> None:
        super().__init__(*args)