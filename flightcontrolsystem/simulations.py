""" Simulators for each sensor """
import random
from flightsimexception import *

#each instance of the simulation needs to be global to be
#accessed by program.  If the classes were instantiated elsewhere
#this would not be needed.  This simulation emulates hardware
#so this becomes an association/aggrigation
global tach1_
global tach2_
global compass_
global engTemp1_
global engTemp2_
global fuelL_
global fuelC_
global fuelR_
global airspeed_

global UPDATE_PERIOD
UPDATE_PERIOD = 200


class Simulator:
    """Base Class for all simulators"""

    def __init__(self,devID:str,initVal:float) -> None:
        """ all simulations will require a devID, the GUI ID of the 
        input 'device' as a string;
        and an initial value as a default output"""
        self.devID = devID
        self.value = initVal # default value

    def update(self,values:dict) -> int:
        """values is the dictionary of values of which the devID is the key
        Base Class Simulation just returns initVal from constructor
        Need to override to get simulated data"""
        return self.value

class Compass(Simulator):
    '''builds compass and sets the stage for changing values; drifts and must be realigned on occasion'''
    _value = 23
    
    def __init__(self, devID:str, initVal:float) -> None:
        '''
        sets the initial compass values
        '''
        super().__init__(devID, initVal)
        Compass._value = initVal

    def update(self, values:dict = None) -> int:
        '''
        Updates compass's class value with jitter plus a drift, then keeps it within 0-360 degrees
        '''
        delta = random.uniform(-0.5, 0.5) + 0.005
        Compass._value = (delta + Compass._value)
        if Compass._value >= 360:
            Compass._value -= 360
        elif Compass._value < 0:
            Compass._value += 360
        return int(Compass._value)
        

    @classmethod
    def setCompass(cls, direction:int) -> None:
        cls._value = direction

class DelaySim(Simulator):
    '''
    Creates a filter which causes lag/delay in the response
    '''
    def __init__(self, devID: str, initVal: float, outLow:float, outHigh:float, delay:int) -> None:
        '''DevID is input range 0-100, outLow and outHigh define output range, delay in milliseconds'''
        super().__init__(devID, initVal)
        self._delay = delay
        numback = delay//UPDATE_PERIOD
        numback = max(numback, 1)
        self.backvalues = [initVal] * numback
        self.slope = (outHigh - outLow) / 100
        self.offset = outLow / self.slope

    def update(self, values:dict) -> float:
        '''adds new val to the filter and returns the filtered value'''
        val = (values[self.devID] - self.offset) * self.slope
        self.backvalues.pop(0)
        self.backvalues.append(val)
        self.value = (sum(self.backvalues) / len(self.backvalues))
        return self.value

    @property
    def delay(self):
        return self._delay

    @delay.setter
    def delay(self, newDelay):
        self._delay = newDelay
        numback = self._delay//UPDATE_PERIOD
        numback = max(numback, 1)
        self.backvalues = [self.value] * numback

class Fuel(Simulator):
    '''
    The simulation will 'burn' fuel at a rate depending on how much the throttle(s) are pushed.
    The more throttle, the faster the fuel is used.
    '''
    BURN_RATE = 0.002 #pounds

    def __init__(self, devID:str, initVal:float, throttle:str=None) -> None:
        '''establishes a Fuel sim object with an optional throttle parameter, and scales it for the GUI'''
        super().__init__(devID, initVal)
        self.throttle = throttle
        self.total = initVal // 100

    def update(self, values:dict) -> float:
        '''calculates the new scaled value to return to the GUI by averaging the throttle input and multiplying
        by the burn rate'''
        if self.throttle != None:
            self.throttleAverage = (values[self.devID] + values[self.throttle]) / 2
        else:
            self.throttleAverage = values[self.devID]

        self.total -= Fuel.BURN_RATE * self.throttleAverage
        returnValue = self.total

        return returnValue

    @classmethod
    def changeBurn(cls, newRate:float) -> None:
        cls.BURN_RATE = newRate

#Setting the global variable for the compass
initHeading = 23.0
compass_ = Compass('-COMPASS-', initHeading)

#Setting the global variables for the tachometer
DELAY = 1500 #ms
INITIAL = 600 #RPM
LOW_RANGE = 0
HIGH_RANGE = 3850.0 #RPM max output
tach1_ = DelaySim("-THR1-", INITIAL, LOW_RANGE, HIGH_RANGE, DELAY)
tach2_ = DelaySim("-THR2-", INITIAL, LOW_RANGE, HIGH_RANGE, DELAY)

#Setting the global variables for the engine temperature
DELAY = 10000 #ms
INITIAL = 0.15 #volts
LOW_RANGE = 0.0 #volts = -30 degrees F
HIGH_RANGE = 1.3 #volts = 250 degrees F
engTemp1_ = DelaySim("-THR1-", INITIAL, LOW_RANGE, HIGH_RANGE, DELAY)
engTemp2_ = DelaySim("-THR2-", INITIAL, LOW_RANGE, HIGH_RANGE, DELAY)

#Setting the global variables for the fuel gauge displays
INIT_CENTER = 9000
INIT_WING = 8000
fuelL_ = Fuel("-THR1-", INIT_WING, "-THR2-")
fuelC_ = Fuel("-THR1-", INIT_CENTER, "-THR2-")
fuelR_ = Fuel("-THR1-", INIT_WING, "-THR2-")